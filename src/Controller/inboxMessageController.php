<?php

namespace Drupal\ms_react\Controller;

use Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;

/**
 * Class inboxMessageController.
 */
class inboxMessageController extends ControllerBase {

  /**
   * The Source database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $injected_database;

  /**
   * Construct a new inboxMessageController class.
   */
  public function __construct(Connection $injected_database) {
    $this->injected_database = $injected_database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Inboxmessage.
   *
   * @return string
   *   Return Hello string.
   */
  public function InboxMessage() {
    $current_user = \Drupal::currentUser();
    $uid = $current_user->id();
    $header_table = [
      'uid' => $this->t('message id'),
      'userSend' => $this->t('between you'),
      'is_new' => $this->t('message'),
      'lastTime' => $this->t('Send the first message'),
      'creatTime' => $this->t('updated Message'),
    ];
    $query = $this->injected_database->select('ms_react_index', 'ms');
    $query->join('ms_react_sender', 'mrs', 'ms.m_id = mrs.mid');
    $query->fields('ms', [
      'm_id',
      'us_seId',
      'us_reId',
      'is_new',
      'cr_time',
      'up_time',
    ]);
    $query->fields('mrs', ['s_id', 'us_id']);
    $query->condition('mrs.ur_id', $uid, "=");
    $query->orderBy('up_time', 'DESC');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($result as $data) {
      $query = $this->injected_database->select('ms_react_message', 'sm');
      $query->fields('sm', ['ms_id', 'mid', 'author']);
      $query->condition('sm.mid', $data['m_id'], "=");
      $query->orderBy('ms_id', 'DESC');
      $query->range(0, 1);
      $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
      $userReceive = '';
      if (empty($data['role_name'])) {
        $users = User::load($data['us_id']);
        $userReceive = $users->realname;
      }
      else {
        $userReceive = $data['role_name'] . ' ' . $this->t('role');
      }
      $isNew = '';
      if ($current_user->id() != $results[0][author]) {
        if ($data['is_new'] == 1) {
          $isNew = $this->t('new');
        }
      }
      $creatTime = $this->t('@time ago', ['@time' => \Drupal::service('date.formatter')->formatTimeDiffSince($data['cr_time'])]);
      $updateTime = $this->t('@time ago', ['@time' => \Drupal::service('date.formatter')->formatTimeDiffSince($data['up_time'])]);
      $messageId = Url::fromUserInput('/ms/chat/' . $data['m_id']);
      $rows[] = [
        Link::fromTextAndUrl($data['m_id'], $messageId),
        'userReceive' => $userReceive,
        Link::fromTextAndUrl(count($results) . ' ' . $isNew, $messageId),
        'cr_time' => $creatTime,
        'up_time' => $updateTime ,
      ];
    }
    /** @var row $rows */
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => $this->t('No send message'),
    ];
    return $form;
  }

  /**
   * Returns a page title.
   */
  public function getTitle() {
    return $this->t('message inbox');
  }

}
